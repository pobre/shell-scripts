read -p "¿Cuál es tu nota?: " nota

while [ $nota -le 0 -o $nota -gt 10 ]; do
read -p "¿Cuál es tu nota?: " nota
done

if [ $nota -lt 5 ]; then
echo "Estas suspendido"
elif [ $nota -eq 5 ]; then 
echo "Estas aprobado"
elif [ $nota -eq 6 ]; then
echo "Tienes un bien"
elif [ $nota -eq 7 ] || [ $nota -eq 8 ]; then
echo "Tienes un notable"
elif [ $nota -eq 9 ] || [ $nota -eq 10 ]; then
echo "Tienes un sobresaliente"
fi
