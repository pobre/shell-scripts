
#!/bin/bash

read -p "Escribe el primer número: " numero1
read -p "Escribe el segundo número: " numero2

if [ $numero1 -lt $numero2 ]; then
echo "$numero1 es menor que $numero2"
elif [ $numero1 -eq $numero2 ]; then
echo "$numero1 y $numero2 son iguales"
else
echo "$numero1 es mayor que $numero2"
fi
