#!/bin/bash

read -p "Escribe un número del 1 al 30: " dia

while [ $dia -lt 1 -o $dia -gt 30 ]; do
read -p "Escribe un número del 1 al 30: " dia
done

if [ $dia -gt 0 -a $dia -le 7 ]; then
semana=1
elif [ $dia -gt 7 -a $dia -le 14 ]; then
semana=2
elif [ $dia -gt 14 -a $dia -le 21 ]; then
semana=3
elif [ $dia -gt 21 -a $dia -le 28 ]; then
semana=4
elif [ $dia -gt 28 -a $dia -le 30 ]; then
semana=5
fi

if [ $semana -eq 5 ]; then
dia=$(($dia-28))
elif [ $semana -eq 4 ]; then
dia=$(($dia-21))
elif [ $semana -eq 3 ]; then
dia=$(($dia-14))
elif [ $semana -eq 2 ]; then
dia=$(($dia-7))
fi

if [ $dia -eq 1 ]; then
echo "Lunes"
elif [ $dia -eq 2 ]; then
echo "Martes"
elif [ $dia -eq 3 ]; then
echo "Miercoles"
elif [ $dia -eq 4 ]; then
echo "Jueves"
elif [ $dia -eq 5 ]; then
echo "Viernes"
elif [ $dia -eq 6 ]; then
echo "Sabado"
elif [ $dia -eq 7 ]; then
echo "Domingo"
fi
