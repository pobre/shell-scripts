#!/bin/bash

read -p "Cuantos litros quieres?: " litros

if [ $litros -le 50 ]; then
suma=$(echo "escale=2; $litros*0.4" | bc)
echo "El costo de $litros litros es de $suma euros"
fi

if [ $litros -gt 50 -a $litros -le 200 ]; then
suma2=$(($litros-50))
suma3=$(echo "escale=2; $suma2*0.2 " | bc)
suma4=$(echo "escale=2; 20+$suma3" | bc)
echo "El costo de $litros litros es de $suma4 euros"
fi

if [ $litros -gt 200 ]; then
suma5=$(($litros-200))
suma6=$(echo "escale=2; $suma5*0.1" | bc)
suma7=$(echo "escale=2; 50+$suma6 " | bc)
echo "El costo de $litros litros es de $suma7 euros"
fi
